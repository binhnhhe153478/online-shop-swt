﻿
USE master
GO
alter database PRJ301_SP2022
 set single_user with rollback immediate
if exists (select * from sysdatabases where name='PRJ_Assignment') drop database PRJ_Assignment1
GO

CREATE DATABASE PRJ301_SP2022

GO

USE [PRJ301_SP2022]
GO

/****** Object:  Table [dbo].[category]    Script Date: 13-02-2022 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[category_HE153478_SE1606](
	[cid] [int] NOT NULL,
	[cname] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
INSERT [dbo].[category_HE153478_SE1606] ([cid], [cname]) VALUES (2, N'Đàn Ghitar')
INSERT [dbo].[category_HE153478_SE1606] ([cid], [cname]) VALUES (3, N'Đàn Ukulele')
INSERT [dbo].[category_HE153478_SE1606] ([cid], [cname]) VALUES (4, N'Phụ Kiện')

--

/****** Object:  Table [dbo].[account]    Script Date: 18:53:22 ******/
SET ANSI_NULLS ON
 GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_HE153478_SE1606]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[account_HE153478_SE1606](
	[username] [varchar](150) NOT NULL,
	[pass] [varchar](150) NOT NULL,
	[isAdmin] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'guest', N'1', 0)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'Nguyen Van chuc', N'1', 1)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'haothien', N'123', 0)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'huuhuan', N'1', 0)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'huyanh', N'1', 1)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'namnguyen', N'1', 0)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'phamtuan', N'123', 0)
INSERT [dbo].[account_HE153478_SE1606]([username], [pass], [isAdmin]) VALUES (N'tintin', N'1', 0)
INSERT [dbo].[account_HE153478_SE1606] ([username], [pass], [isAdmin]) VALUES (N'vanchuc', N'1', 0)
/****** Object:  Table [dbo].[order]    Script Date:  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[order_HE153478_SE1606]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[order_HE153478_SE1606](
	[orderid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](150) NOT NULL,
	[Cname] [nvarchar](50) NOT NULL,
	[address] [nvarchar](max) NOT NULL,
	[total] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[order_HE153478_SE1606] ON
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (1, N'Nguyen Van chuc', N'Nguyen Van chuc', N'nghe an', 750000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (2, N'huuhuan', N'Nguyen Van Chuc', N'ha noi', 890000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (3, N'haothien', N'Nguyen Hao Hao', N'Hai Duong', 3560000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (5, N'guest', N'Anh Da Den', N'hoai duc', 960000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (6, N'vanchuc', N'Test', N'Test', 7250000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (7, N'huyanh', N'nguyen huy anh', N'Thach hoa', 1420000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (8, N'namnguyen', N'nguyen van khong', N'yen thanh', 1520000.0000)
INSERT [dbo].[order_HE153478_SE1606] ([orderid], [username], [Cname], [address], [total]) VALUES (9, N'tintin', N'nguyen van khong', N'phu dien', 1020000.0000)
SET IDENTITY_INSERT [dbo].[order_HE153478_SE1606] OFF
/****** Object:  Table [dbo].[product]    Script Date:  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[product_HE153478_SE1606]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[product_HE153478_SE1606](
	[id] [int] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[image] [varchar](max) NOT NULL,
	[price] [money] NOT NULL,
	[quantity] [int] NOT NULL,
	[title] [varchar](max) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[cid] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (1, N'G001-SL', N'https://images.pexels.com/photos/1010519/pexels-photo-1010519.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260', 320000, 2,N'Phiên bản SPEM01', N'Là gỗ nhập khẩu cao cấp,Top gỗ thông nguyên tấm, lưng và hông đàn gỗ Ưalnut nhập khẩu dày dặn cho ra tiếng bass , treb rõ ràng. Chuẩn nốt ', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (2, N'G002-SL', N'https://images.pexels.com/photos/1656415/pexels-photo-1656415.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 250000, 0,N'Phiên bản SPEM02',  N' Phiên bản SPEM- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ ép nhập khẩu( sạch và mịn- vang hơn các loại ván ép trên thị trường)', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (3, N'G003-SL', N'https://images.pexels.com/photos/1407322/pexels-photo-1407322.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 220000, 10,N'Phiên bản SPEM03',  N' Là gỗ nhập khẩu cao cấp,Top gỗ thông nguyên tấm, lưng và hông đàn gỗ Ưalnut nhập khẩu dày dặn cho ra tiếng bass , treb rõ ràng. Chuẩn nốt', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (4, N'G004-SL', N'https://images.pexels.com/photos/3428498/pexels-photo-3428498.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 180000, 1,N'Phiên bản SPEM04',  N' Phiên bản VG-HD- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ hồng đào nguyên tấm- khóa đúc- nếu quý khách muốn cây đàn tốt hơn, âm hay hơn, càng chơi lâu càng hay hãy chọn VG-HD', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (5, N'G005-SL', N'https://images.pexels.com/photos/2156327/pexels-photo-2156327.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 350000, 3,N'Phiên bản SPEM05',  N'Phiên bản VG-Ko1- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ Koa, loại tốt Vinaguitar khuyên các bạn nên mua ', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (6, N'G006-SL', N'https://images.pexels.com/photos/2909367/pexels-photo-2909367.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 420000, 6,N'Phiên bản SPEM06',  N'Sản phẩm và phân phối bởi Vinaguitar: thợ chuyên nghiệp, tay nghề tốt, mang lại cho quý khách cây đàn chuẩn, bảo hành tốt nhất', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (7, N'G007-SL', N'https://images.pexels.com/photos/2646825/pexels-photo-2646825.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 520000, 12,N'Phiên bản SPEM07',  N' Là gỗ nhập khẩu cao cấp,Top gỗ thông nguyên tấm, lưng và hông đàn gỗ Ưalnut nhập khẩu dày dặn cho ra tiếng bass , treb rõ ràng. Chuẩn nốt', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (8, N'G008-SL', N'https://images.pexels.com/photos/625788/pexels-photo-625788.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 540000, 12,N'Phiên bản SPEM08',  N'Phù hợp với các lứa tuổi: đàn được thiết kế dáng A tiêu chuẩn, giúp cho mọi lứa tuổi, mọi người chơi dễ dáng sử dụng và tiếp cận', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (9, N'G009-SL', N'https://images.pexels.com/photos/258283/pexels-photo-258283.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 599000, 12,N'Phiên bản SPEM09',  N' Phiên bản SPEM- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ ép nhập khẩu( sạch và mịn- vang hơn các loại ván ép trên thị trường)', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (10, N'G010-SL', N'https://images.pexels.com/photos/3714523/pexels-photo-3714523.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 115000, 1,N'Phiên bản SPEM10',  N' Phiên bản VG-HD- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ hồng đào nguyên tấm- khóa đúc- nếu quý khách muốn cây đàn tốt hơn, âm hay hơn, càng chơi lâu càng hay hãy chọn VG-HD', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (11, N'G011-SL', N'https://images.pexels.com/photos/7558106/pexels-photo-7558106.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 250000, 12,N'Phiên bản SPEM11',  N' Là gỗ nhập khẩu cao cấp,Top gỗ thông nguyên tấm, lưng và hông đàn gỗ Ưalnut nhập khẩu dày dặn cho ra tiếng bass , treb rõ ràng. Chuẩn nốt', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (12, N'G012-SL', N'https://images.pexels.com/photos/9057763/pexels-photo-9057763.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 775000, 12,N'Phiên bản SPEM12',  N'Phiên bản SPEM- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ ép nhập khẩu( sạch và mịn- vang hơn các loại ván ép trên thị trường)', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (13, N'G013-SL', N'https://images.pexels.com/photos/1716580/pexels-photo-1716580.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 995000, 12,N'Phiên bản SPEM13',  N' Là gỗ nhập khẩu cao cấp,Top gỗ thông nguyên tấm, lưng và hông đàn gỗ Ưalnut nhập khẩu dày dặn cho ra tiếng bass , treb rõ ràng. Chuẩn nốt', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (14, N'G014-SL', N'https://images.pexels.com/photos/2016810/pexels-photo-2016810.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 150000, 12,N'Phiên bản SPEM14',  N'Phiên bản VG-Ko1- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ Koa, loại tốt Vinaguitar khuyên các bạn nên mua ', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (15, N'G015-SL', N'https://images.pexels.com/photos/8586560/pexels-photo-8586560.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 150000, 12,N'Phiên bản SPEM15',  N' Phiên bản VG-HD- mặt đàn gỗ thông nguyên tấm- lưng hông gỗ hồng đào nguyên tấm- khóa đúc- nếu quý khách muốn cây đàn tốt hơn, âm hay hơn, càng chơi lâu càng hay hãy chọn VG-HD', 2)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (16, N'U01-OL', N'https://images.pexels.com/photos/346709/pexels-photo-346709.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 140000, 12,N'Phiên bản SPEM16',  N' Đàn Ukulele Soprano size 01 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay.', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (17, N'U02-OL', N'https://images.pexels.com/photos/4215468/pexels-photo-4215468.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 190000, 10,N'Phiên bản SPEM17',  N'Đàn Ukulele Soprano size 02 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay. ', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (18, N'U03-OL', N'https://images.pexels.com/photos/3944101/pexels-photo-3944101.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 180000, 12,N'Phiên bản SPEM18',  N'Đàn Ukulele Soprano size 03 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay. ', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (19, N'U04-OL', N'https://images.pexels.com/photos/6933425/pexels-photo-6933425.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 170000, 12,N'Phiên bản SPEM19',  N' Đàn Ukulele Soprano size 04 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay.', 3)
INSERT [dbo].[product_HE153478_SE1606]([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (20, N'U05-OL', N'https://images.pexels.com/photos/3975591/pexels-photo-3975591.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 160000, 12,N'Phiên bản SPEM20',  N'Đàn Ukulele Soprano size 05 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay. ', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (21, N'U06-OL', N'https://images.pexels.com/photos/346728/pexels-photo-346728.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 190000, 12,N'Phiên bản SPEM21',  N' Đàn Ukulele Soprano size 06 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay.', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (22, N'U07-OL', N'https://images.pexels.com/photos/4243879/pexels-photo-4243879.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500', 110000, 12,N'Phiên bản SPEM22',  N'Đàn Ukulele Soprano size 07 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay.', 3)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (23, N'U08-OL', N'https://images.pexels.com/photos/3975587/pexels-photo-3975587.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 150000, 12,N'Phiên bản SPEM23',  N'Size Sprano nhỏ gọn, mặt đàn màu gỗ thông, hông và lưng màu đỏ hồng  Đàn Ukulele Soprano size 08 của Piano Lovers được làm từ gỗ ép nguyên tấm, âm thanh hay.', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (24, N'P01-DL', N'https://cf.shopee.vn/file/208b82b7d2ee4bd8163474c0bc568ecd', 500000, 12,N'Phiên bản SPEM24',  N'- Dây đeo đàn guitar này cần phải gắn chốt đeo, 2 chốt ở cần đàn và dưới đáy thùng giúp đeo dây chắc chắn.- Vải mềm đeo êm vai với độ dài phù hợp- Chất liệu : Vải Polyester- Xuất xứ : Đài Loan- Phân loại: Đa dạng mẫu cho các bạn lựa chọn thoải mái', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (25, N'P02-DL', N'https://nhaccutienmanh.vn/wp-content/uploads/2019/10/day-deo-guitar-daddario-pws100-44mm-black-avt.jpg', 100000, 12,N'Phiên bản SPEM25',  N'Dây đeo đàn guitar màu trơn( đen/ nâu, xám) : là loại thường, chất liệu vải dù 2 lớp rất bền, dòng này cho các bạn thích 1 màu, có bọc da 1 đầu', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (26, N'P03-DL', N'http://nhaccuvanxuan.vn/wp-content/uploads/2015/12/day-deo-dan-fender.jpg', 300000 ,12,N'Phiên bản SPEM26',  N'Dây đeo đàn guitar nhiều màu: chất liệu vải xịn, đan 3 lớp dày, đẹp hơn dây đeo màu trơn, nhiều họa tiết tinh tế phù hợp với sở thích mỗi bạn.', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (27, N'P04-DL', N'https://guitar.station.vn/wp-content/uploads/2017/02/day-deo-ukulele-1.png', 900000, 12,N'Phiên bản SPEM27',  N'Size: 88-146/5.2cm( dài/ rộng- chiều dài có thể thay đổi phù hợp với người chơi', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (28, N'P05-DL', N'https://cf.shopee.vn/file/8e85ad02e3eaf74e23aae4d195a23d67', 200000, 12,N'Phiên bản SPEM28',  N'Dây đeo ukulele thổ cẩm kiểu mới, không cần đục đàn, có chốt móc trực tiếp cực tiện lợi', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (29, N'P06-DL', N'https://vietthuonghanoi.com/wp-content/uploads/2017/11/d-addario-50-a12-1-600x600.jpg', 990000, 12,N'Phiên bản SPEM29',  N'Chất liệu dây mềm mại, không bị chà sát hay gây ngứa cổ như các loại dây thường khác.', 4)
INSERT [dbo].[product_HE153478_SE1606] ([id], [name], [image], [price], [quantity], [title], [description], [cid]) VALUES (30, N'P07-DL', N'https://canary.contestimg.wish.com/api/webimage/5c9543744578cb33ad0f56cd-large.jpg?cache_buster=ed84658c453c742ad13b9d565566ed06', 990000, 12,N'Phiên bản SPEM30',  N' dây thường.', 4)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[orderdetail_HE153478_SE1606]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[orderdetail_HE153478_SE1606](
	[orderid] [int] NOT NULL,
	[productID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[price] [money] NOT NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (1, 3, 3, 250000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (2, 2, 2, 320000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (2, 2, 1, 250000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (3, 3, 10, 320000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (3, 4, 2, 180000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (7, 4, 2, 180000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (5, 3, 3, 320000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (6, 2, 29, 250000.0000)
INSERT [dbo].[orderdetail_HE153478_SE1606] ([orderid], [productID], [quantity], [price]) VALUES (7, 3, 3, 320000.0000)
/****** Object:  ForeignKey [FK__order__username__0EA330E9]    Script Date:******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__order__username__0EA330E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[order_HE153478_SE1606]'))
ALTER TABLE [dbo].[order_HE153478_SE1606]  WITH CHECK ADD FOREIGN KEY([username])
REFERENCES [dbo].[account_HE153478_SE1606] ([username])
GO
/****** Object:  ForeignKey [FK__product__cid__08EA5793]    Script Date: ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__product__cid__08EA5793]') AND parent_object_id = OBJECT_ID(N'[dbo].[product_HE153478_SE1606]'))
ALTER TABLE [dbo].[product_HE153478_SE1606]  WITH CHECK ADD FOREIGN KEY([cid])
REFERENCES [dbo].[category_HE153478_SE1606] ([cid])
GO
/****** Object:  ForeignKey [FK__orderdeta__order__108B795B]    Script Date: ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__orderdeta__order__108B795B]') AND parent_object_id = OBJECT_ID(N'[dbo].[orderdetail_HE153478_SE1606]'))
ALTER TABLE [dbo].[orderdetail_HE153478_SE1606]  WITH CHECK ADD FOREIGN KEY([orderid])
REFERENCES [dbo].[order_HE153478_SE1606] ([orderid])
GO
/****** Object:  ForeignKey [FK__orderdeta__produ__117F9D94]    Script Date:  ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__orderdeta__produ__117F9D94]') AND parent_object_id = OBJECT_ID(N'[dbo].[orderdetail_HE153478_SE1606]'))
ALTER TABLE [dbo].[orderdetail_HE153478_SE1606]  WITH CHECK ADD FOREIGN KEY([productID])
REFERENCES [dbo].[product_HE153478_SE1606] ([id])
GO
